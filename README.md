Bornfight MVP based template app

It contains common classes which will enforce the use of MVP architecture.

Activities must extend BaseActivity.
BaseActivity contains generic for presenter, which will extend BasePresenter class.
Finally, there is BaseView interface which all activities must implement.
Besides BaseActivity, BaseFragment is also available for Fragment extensions.

BaseActivity handles creation and lifecycle of presenters.
Presenters are also cached during the lifecycle changes (screen orientation etc.),
 so presenter data is preserved in such situations.

ButterKnife is used for view binding. ButterKnife will be intialized in BaseActivity's onCreate method. For fragments, ButterKnife must be initialized by developer in onCreateView method.

Template also contains basic Retrofit setup. ApiService class initializes Retrofit, adds logging interceptor for DEBUG versions, and it also adds custom SocketFactory for API version < 21. Bornfight servers support only TLS protocols, so by this custom factory, these are enforced.

There's a demo package for the new app, called "appname". Use Android Studio refactoring options to rename it according to your new app name.
Also, be reminded that you have to change applicationId in build.gradle file, also according to your new app name.