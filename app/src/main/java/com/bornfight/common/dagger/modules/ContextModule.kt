package com.bornfight.common.dagger.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ianic on 09/12/16.
 */

@Module
class ContextModule(private val context: Context) {

    @Provides
    @Singleton
    fun context(): Context {
        return context
    }

}
