package com.bornfight.common.dagger.modules

import com.bornfight.appname.BuildConfig
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.data.retrofit.RxErrorHandlingCallAdapterFactory
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.utils.GsonUtil
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Singleton

/**
 * Created by ianic on 08/12/16.
 */

@Module(includes = [(NetworkModule::class)])
class ApiServiceModule {

    @Provides
    @Singleton
    fun apiService(retrofit: Retrofit): ApiInterface {
        return retrofit.create(ApiInterface::class.java)
    }

    @Provides
    @Singleton
    fun retrofit(client: OkHttpClient, sessionPref: SessionPrefImpl): Retrofit {

        val gson = GsonUtil.defaultGson

        val baseUrl = sessionPref.apiBaseUrl()

        return Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl ?: BuildConfig.BASE_API_URL)
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
    }
}
