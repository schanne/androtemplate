package com.bornfight.common.dagger.modules

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment

import com.bornfight.appname.fragmentexample.ExampleFragmentContract
import com.bornfight.appname.fragmentexample.ExampleFragmentPresenter
import com.bornfight.common.dagger.qualifiers.ActivityContext

import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by tomislav on 17/02/2017.
 */

@Module
class FragmentModule(private val mFragment: Fragment) {

    @Provides
    @ActivityContext
    internal fun provideContext(): Context? {
        return mFragment.activity
    }

    @Provides
    internal fun provideActivity(): Activity? {
        return mFragment.activity
    }

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Provides
    internal fun examplePresenter(presenter: ExampleFragmentPresenter): ExampleFragmentContract.Presenter<ExampleFragmentContract.View> {
        return presenter
    }

}
