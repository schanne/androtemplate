package com.bornfight.common.dagger.modules

import android.content.Context
import com.bornfight.appname.BuildConfig
import com.bornfight.common.session.SessionPrefImpl
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by ianic on 08/12/16.
 * Updated by tsmrecki on 01/03/19.
 */

@Module(includes = [ContextModule::class])
class NetworkModule {

    @Provides
    @Singleton
    fun requestInterceptor(sessionPref: SessionPrefImpl): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .header("Accept", "application/json")
                .method(original.method(), original.body())

            // add Authorization token to every request except /login requests
            // this is used because of the poor implementation of Basic Auth on BF server
            if (sessionPref.token.isNotEmpty() && !chain.request().url().pathSegments().contains("login")) {
                requestBuilder.addHeader("Authorization", sessionPref.token)
            }

            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }

    @Provides
    @Singleton
    fun okHttpClient(requestInterceptor: Interceptor, context: Context): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.addInterceptor(requestInterceptor)

        // add logging interceptor for DEBUG variants
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(interceptor)
        }

        // TODO: enable if local network cache required
        // addCacheInterceptor(context, client)

        // This is used to force TLSv1.1 & TLSv1.2 protocols on OS prior Lollipop
        // because the BF server supports only these on older versions
        /* TODO uncomment if targeting version prior Lollipop
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                val socketFactoryCompat = TLSSocketFactoryCompat()
                client.sslSocketFactory(TLSSocketFactoryCompat(),
                    Platform.get().trustManager(socketFactoryCompat))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        */

        return client.build()
    }

    private fun addCacheInterceptor(context: Context, client: OkHttpClient.Builder) {
        val cacheInterceptor = Interceptor { chain ->
            val response = chain.proceed(chain.request())
            val cacheControl = CacheControl.Builder()
                .maxAge(2, TimeUnit.MINUTES)
                .build()

            response.newBuilder()
                .header("Cache-Control", cacheControl.toString())
                .build()
        }

        val cache = Cache(File(context.cacheDir, "http-cache"), (10 * 1024 * 1024).toLong())

        client.addNetworkInterceptor(cacheInterceptor)
        client.cache(cache)
    }
}
