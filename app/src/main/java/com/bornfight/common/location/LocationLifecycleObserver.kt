package com.bornfight.common.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.utils.LocationUtil
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices

/**
 * Created by ianic on 01/07/2019.
 *
 * This class works together with [LocationFragment]; it implements [LifecycleObserver]
 * and updates the current location in accordance with the current state of the
 * activity/fragment state.
 */

class LocationLifecycleObserver(
    private val listener: LocationListener,
    private val lifecycle: Lifecycle,
    private val session: SessionPrefImpl? = null //if null, won't be used
) : LifecycleObserver,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mLocation: Location? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        mGoogleApiClient = GoogleApiClient.Builder(listener.mContext,
            this, this)
            .addApi(LocationServices.API)
            .build()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() = mGoogleApiClient.connect()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() = mLocation?.let {
        listener.onLocationReady(it)
    } ?: getLocation()

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() = mGoogleApiClient.disconnect()

    fun getLocation() {
        if (ActivityCompat.checkSelfPermission(listener.mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            return
        }

        LocationServices.getFusedLocationProviderClient(listener.mContext)
            .lastLocation.addOnSuccessListener {
            mLocation = it

            // if location unavailable, but the cached version saved, take the cached version instead, but notify the location is unavailable
            if (mLocation == null && session?.lastKnownUserLatLng != null) {
                val tempLocation = Location(LocationManager.GPS_PROVIDER)
                tempLocation.latitude = session.lastKnownUserLatLng!!.latitude
                tempLocation.longitude = session.lastKnownUserLatLng!!.longitude
                mLocation = tempLocation

                if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                    this.listener.onLocationUnavailable()
            }

            mLocation?.let { location ->
                session?.lastKnownUserLatLng = LocationUtil.latLngFromLocation(location)
            }

            if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                mLocation?.let { location ->
                    listener.onLocationReady(location)
                }
                    ?: listener.onLocationUnavailable()
        }
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)) {
            listener.onLocationUnavailable()
        }
    }
}

interface LocationListener {
    fun onLocationReady(lastKnownLocation: Location)
    fun onLocationUnavailable()
    val mContext: Context
}