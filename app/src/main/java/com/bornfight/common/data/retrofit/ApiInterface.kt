package com.bornfight.common.data.retrofit

import android.text.TextUtils
import com.bornfight.appname.BuildConfig
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.*
import java.util.*

/**
 * Created by tomislav on 26/02/16.
 */

interface ApiInterface {

    //users
    @FormUrlEncoded
    @POST("users/forgotten")
    fun forgotPassword(@Field("email") email: String): Single<Void>

    @FormUrlEncoded
    @POST("users/reset")
    fun passwordReset(@Field("password_reset_token") token: String, @Field("new_password") password: String): Single<Void>

    @GET("users/confirm")
    fun confirmRegister(@Query("verification_token") verificationToken: String): Single<ConfirmRegistrationResponse>

    @POST("users/login")
    fun login(@Header("Authorization") accessToken: String): Single<LoginResponse>

    //firebase
    @POST("firebases")
    @FormUrlEncoded
    fun saveFirebaseId(@Field("registration_id") registrationId: String, @Field("platform") platform: String): Single<Void>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = BuildConfig.BASE_API_URL + "firebases/reg-delete", hasBody = true)
    fun deleteFirebaseId(@Field("registration_id") registrationId: String): Single<Void>

    //bugreport
    @POST("feedback")
    @FormUrlEncoded
    fun sendReport(@Field("text") reportDescription: String): Single<Void>

    class LoginResponse {
        @Expose
        @SerializedName("access_token")
        var accessToken: String? = null
        /*
        @Expose
        @SerializedName("user")
        public User user;
        */

        class Error {
            @Expose
            @SerializedName("email")
            var usernameError: String? = null
            @Expose
            @SerializedName("password")
            var passwordError: String? = null
        }
    }


    class ConfirmRegistrationResponse {
        /*
        @Expose
        @SerializedName("user")
        public User user;
        */
        @Expose
        @SerializedName("access_token")
        var token: String = ""
    }

    class GeneralError {
        @Expose
        @SerializedName("message")
        var message: String = ""
    }

    class ValidationErrors {
        @Expose
        @SerializedName("errors")
        internal var errors: List<ValidationError> = ArrayList()

        fun getFieldErrors(fieldName: String): List<String> {
            for (ve in errors) {
                if (ve.field == fieldName) {
                    val messages = ve.messages?.toList()
                    return if (messages?.isNotEmpty() == true) {
                        messages
                    } else {
                        listOf(ve.message)
                    }
                }
            }
            return emptyList()
        }

        fun hasErrors(): Boolean {
            return !errors.isEmpty()
        }

        fun getFieldError(fieldName: String, listDelimiter: CharSequence): String {
            for (ve in errors) {
                if (ve.field == fieldName) {
                    return if (ve.messages?.isNotEmpty() == true) {
                        TextUtils.join(listDelimiter, ve.messages)
                    } else {
                        ve.message
                    }
                }
            }
            return ""
        }

        fun getErrors(): String {
            val stringBuilder = StringBuilder()
            for (ve in errors) {
                if (ve.messages?.isNotEmpty() == true) {
                    stringBuilder.append(TextUtils.join(", ", ve.messages)).append("\n")
                } else {
                    stringBuilder.append(ve.message).append("\n")
                }
            }
            return stringBuilder.toString()
        }

        fun setErrors(errors: List<ValidationError>) {
            this.errors = errors
        }

        inner class ValidationError {
            @SerializedName("field")
            internal var field = ""
            @SerializedName("messages")
            internal var messages: List<String>? = emptyList()
            @SerializedName("message")
            internal var message = ""
        }
    }


}


