package com.bornfight.common.mvp

import androidx.annotation.StringRes

import com.bornfight.common.data.retrofit.ApiInterface

/**
 * Created by tomislav on 17/02/2017.
 */

class BaseContract {

    interface Presenter<TView : BaseContract.View> {

        fun subscribe(view: TView)

        fun unsubscribe()

        fun handleError(e: Throwable)

        fun handleError(e: Throwable, validationErrorsListener: OnValidationErrorsListener?)

        interface OnValidationErrorsListener {
            fun onValidationErrors(errors: ApiInterface.ValidationErrors)
        }

        fun logout()

    }

    interface View {

        fun showProgressCircle(show: Boolean)

        fun showError(errorMessage: String)

        fun showError(stringResourceId: Int)

        fun showLoader(show: Boolean)

        fun showLoader(show: Boolean, cancelable: Boolean)

        fun showShortInfo(info: String)

        fun showShortInfo(stringResourceId: Int)

        fun showInfoDialog(title: String?, description: String?, buttonText: String?)

        fun showInfoDialog(@StringRes titleResourceId: Int, @StringRes descriptionResourceId: Int, @StringRes buttonText: Int)

        fun hideKeyboard()

        fun onLogout()

    }
}
