package com.bornfight.common.mvp.ui

import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import com.bornfight.appname.App
import com.bornfight.common.dagger.components.ActivityComponent
import com.bornfight.common.dagger.components.DaggerFragmentComponent
import com.bornfight.common.dagger.components.FragmentComponent
import com.bornfight.common.dagger.modules.FragmentModule
import com.bornfight.common.mvp.BaseContract
import com.bornfight.common.session.SessionPrefImpl
import javax.inject.Inject


/**
 * Created by tomislav on 16/09/16.
 */
abstract class BaseFragment : DialogFragment(), BaseContract.View {

    @Inject
    lateinit var session: SessionPrefImpl

    protected var baseActivity: BaseActivity? = null
    lateinit var fragmentComponent: FragmentComponent

    protected val activityComponent: ActivityComponent?
        get() = baseActivity?.activityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentComponent = DaggerFragmentComponent.builder()
            .applicationComponent((activity?.application as App).component)
            .fragmentModule(FragmentModule(this))
            .build()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            baseActivity = context
        }
    }

    override fun onLogout() {
        baseActivity?.onLogout()
    }

    override fun showError(errorMessage: String) {
        baseActivity?.showError(errorMessage)
    }


    override fun showShortInfo(info: String) {
        baseActivity?.showShortInfo(info)
    }

    override fun showError(stringResourceId: Int) {
        baseActivity?.showError(stringResourceId)
    }

    override fun showShortInfo(stringResourceId: Int) {
        baseActivity?.showShortInfo(stringResourceId)
    }

    override fun showProgressCircle(show: Boolean) {
        baseActivity?.showProgressCircle(show)
    }

    override fun showLoader(show: Boolean) {
        baseActivity?.showLoader(show)
    }

    override fun showLoader(show: Boolean, cancelable: Boolean) {
        baseActivity?.showLoader(show, cancelable)
    }

    override fun showInfoDialog(title: String?, description: String?, buttonText: String?) {
        baseActivity?.showInfoDialog(title, description, buttonText)
    }

    override fun showInfoDialog(@StringRes titleResourceId: Int, @StringRes descriptionResourceId: Int, @StringRes buttonText: Int) {
        baseActivity?.showInfoDialog(titleResourceId, descriptionResourceId, buttonText)
    }

    override fun hideKeyboard() {
        baseActivity?.hideKeyboard()
    }

    protected fun checkPermissionGranted(permission: String): Boolean {
        return baseActivity?.checkPermissionGranted(permission) == true
    }

    protected fun checkPermissionsGranted(vararg permissions: String): Boolean {
        return baseActivity?.checkPermissionsGranted(*permissions) == true
    }


    protected fun checkPermissionDeniedForGood(permission: String): Boolean {
        return baseActivity?.checkPermissionDeniedForGood(permission) == true
    }
}
