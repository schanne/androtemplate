package com.bornfight.common.session

import android.content.Context
import android.content.SharedPreferences
import com.bornfight.appname.BuildConfig
import com.google.android.gms.maps.model.LatLng
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by tomislav on 16/12/2016.
 */

@Singleton
class SessionPrefImpl @Inject
constructor(context: Context, private val devicePreferences: DevicePreferences) : Session {

    private val mSharedPreferences: SharedPreferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID + "_session", Context.MODE_PRIVATE)

    override val isLoggedIn: Boolean
        get() = mSharedPreferences.getBoolean("logged_in", false)

    override var userId: Long
        get() = mSharedPreferences.getLong("user_id", 0)
        set(userId) = mSharedPreferences.edit().putLong("user_id", userId).apply()

    override var token: String
        get() = mSharedPreferences.getString("session_token", "") ?: ""
        set(token) {
            mSharedPreferences.edit().putString("session_token", "Bearer $token").apply()
        }

    override var lastKnownUserLatLng: LatLng?
        get() {
            return if (!mSharedPreferences.contains("user_last_location_lat") ||
                !mSharedPreferences.contains("user_last_location_lon")) {
                null
            } else {
                LatLng(mSharedPreferences.getFloat("user_last_location_lat", 0f).toDouble(),
                    mSharedPreferences.getFloat("user_last_location_lon", 0f).toDouble())
            }
        }
        set(value) {
            if (value == null) return
            mSharedPreferences.edit().putFloat("user_last_location_lat", value.latitude.toFloat()).apply()
            mSharedPreferences.edit().putFloat("user_last_location_lon", value.longitude.toFloat()).apply()
        }

    override fun setLoggedIn(loggedIn: Boolean, userId: Long) {
        mSharedPreferences.edit()
            .putBoolean("logged_in", loggedIn)
            .putLong("user_id", userId).apply()
    }

    override fun logout() {
        setLoggedIn(false, 0)

        // if user logged out, reset the Firebase token flag so it can
        // be sent again if user logs in back
        devicePreferences.isFirebaseTokenSent = false
        token = ""
    }

    override fun checkPermissionAsked(permission: String): Boolean {
        return mSharedPreferences.contains("PERMISSION_ASKED_$permission")
    }

    override fun setPermissionAsked(permission: String) {
        mSharedPreferences.edit().putBoolean("PERMISSION_ASKED_$permission", true).apply()
    }

    override fun shouldSkipOnboardingStep(stepName: String): Boolean {
        return mSharedPreferences.getBoolean("onboarding_step_skip_$stepName", false)
    }

    override fun setSkipOnboardingStep(stepName: String, skip: Boolean) {
        mSharedPreferences.edit().putBoolean("onboarding_step_skip_$stepName", skip).apply()
    }

    override fun isTipsShown(tipSet: String): Boolean {
        return mSharedPreferences.getBoolean(tipSet + "_tips_shown", false)
    }

    override fun setTipsShown(tipSet: String, shown: Boolean) {
        mSharedPreferences.edit().putBoolean(tipSet + "_tips_shown", shown).apply()
    }

    fun apiBaseUrl(): String? {
        return mSharedPreferences.getString("abu", null)
    }

    fun setApiBaseUrl(baseUrl: String?) {
        if (baseUrl != null) {
            mSharedPreferences.edit().putString("abu", baseUrl).apply()
        } else {
            mSharedPreferences.edit().remove("abu").apply()
        }
    }


}
