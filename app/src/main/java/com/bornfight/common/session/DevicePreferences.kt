package com.bornfight.common.session

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Shared preferences which should not be backed up by Google when app reinstalled.
 * https://developer.android.com/guide/topics/data/autobackup
 */
@Singleton
class DevicePreferences @Inject constructor(context: Context) {

    private val mDevicePrefs: SharedPreferences = context.getSharedPreferences("device_preferences", Context.MODE_PRIVATE)

    var isFirebaseTokenSent: Boolean
        get() = mDevicePrefs.getBoolean("firebase_sent", false)
        set(sent) = mDevicePrefs.edit().putBoolean("firebase_sent", sent).apply()

}