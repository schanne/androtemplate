package com.bornfight.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.text.InputFilter
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StyleRes
import com.bornfight.appname.R
import com.bornfight.common.mvp.ui.BaseActivity
import com.google.android.material.textfield.TextInputLayout
import java.util.*

/**
 * Created by tomislav on 27/05/16.
 * Updated by tomislav on 17/09/18.
 */
object DialogUtil {

    @JvmOverloads
    fun buildInfoDialog(activity: Activity,
                        title: String?,
                        description: String?,
                        buttonText: String?,
                        onConfirmClickListener: View.OnClickListener? = null): Dialog {
        val dialog = Dialog(activity, R.style.Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_info_layout)

        val titleV = dialog.findViewById<TextView>(R.id.dialog_title)
        val descriptionV = dialog.findViewById<TextView>(R.id.dialog_description)

        if (title != null) {
            titleV.text = title
        } else {
            titleV.visibility = View.GONE
        }

        if (description != null) {
            descriptionV.text = description
        } else {
            descriptionV.visibility = View.GONE
        }

        val ok = dialog.findViewById<Button>(R.id.dialog_send)
        if (buttonText != null) {
            ok.text = buttonText
        }

        ok.setOnClickListener { v ->
            dialog.dismiss()
            onConfirmClickListener?.onClick(v)
        }

        return dialog
    }


    fun buildLoaderDialog(activity: Activity, loaderText: String?): Dialog {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_loader)

        val textV = dialog.findViewById<TextView>(R.id.loader_text)

        if (loaderText != null) {
            textV.text = loaderText
        }

        val loaderImage = dialog.findViewById<ImageView>(R.id.loading_img)

        val pulseAnim = ScaleAnimation(1f, 1.2f, 1f, 1.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
            0.5f)
        pulseAnim.repeatCount = Animation.INFINITE
        pulseAnim.repeatMode = Animation.REVERSE
        pulseAnim.duration = 180

        dialog.setOnShowListener { loaderImage.startAnimation(pulseAnim) }

        dialog.setOnDismissListener { loaderImage.clearAnimation() }

        return dialog
    }

    @JvmOverloads
    fun buildInputTextDialog(activity: BaseActivity,
                             @StyleRes theme: Int = R.style.Dialog,
                             title: String,
                             description: String,
                             text: String?,
                             hint: String?,
                             inputType: Int,
                             maxLength: Int,
                             onTextInputDialogListener: OnTextInputDialogListener): Dialog {
        val dialog = Dialog(activity, theme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_input_text_layout)

        val titleV = dialog.findViewById<TextView>(R.id.dialog_title)
        val descriptionV = dialog.findViewById<TextView>(R.id.dialog_description)
        val userNameV = dialog.findViewById<EditText>(R.id.edit_text)

        userNameV.hint = hint
        userNameV.setText(text)
        userNameV.inputType = inputType

        val fArray = arrayOfNulls<InputFilter>(1)
        fArray[0] = InputFilter.LengthFilter(maxLength)
        userNameV.filters = fArray

        titleV.text = title
        descriptionV.text = description

        descriptionV.setOnClickListener { activity.hideKeyboard() }

        val send = dialog.findViewById<Button>(R.id.dialog_send)
        send.setOnClickListener {
            dialog.dismiss()
            onTextInputDialogListener.onTextInput(userNameV.text.toString())
        }

        val cancel = dialog.findViewById<Button>(R.id.dialog_cancel)
        cancel.setOnClickListener {
            onTextInputDialogListener.onSkip()
            dialog.dismiss()
        }

        return dialog
    }

    fun buildNameSurnameDialog(activity: Activity,
                               @StyleRes theme: Int,
                               title: String,
                               description: String,
                               nameHint: String?,
                               surnameHint: String?, inputType: Int,
                               onNameSurnameDialogListener: OnNameSurnameDialogListener): Dialog {
        val dialog = Dialog(activity, theme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_name_surname_layout)

        val titleV = dialog.findViewById<TextView>(R.id.dialog_title)
        val descriptionV = dialog.findViewById<TextView>(R.id.dialog_description)

        val nameInput = dialog.findViewById<TextInputLayout>(R.id.name_input)
        val surnameInput = dialog.findViewById<TextInputLayout>(R.id.surname_input)

        nameInput.editText?.hint = nameHint
        surnameInput.editText?.hint = surnameHint

        nameInput.editText?.inputType = inputType
        surnameInput.editText?.inputType = inputType

        titleV.text = title
        descriptionV.text = description


        val send = dialog.findViewById<Button>(R.id.dialog_send)
        send.setOnClickListener {
            dialog.dismiss()
            onNameSurnameDialogListener.onNameSurnameInput(nameInput.editText?.text.toString().trim { it <= ' ' },
                surnameInput.editText?.text.toString().trim { it <= ' ' })
        }

        val cancel = dialog.findViewById<Button>(R.id.dialog_cancel)
        cancel.setOnClickListener {
            onNameSurnameDialogListener.onSkip()
            dialog.dismiss()
        }

        return dialog
    }

    fun buildAddressDialog(activity: Activity,
                           title: String,
                           address: String,
                           postal: String,
                           city: String,
                           addressDialogListener: AddressDialogListener,
                           onDismissListener: DialogInterface.OnDismissListener): Dialog {
        val dialog = Dialog(activity, R.style.Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_address_data)

        dialog.setOnDismissListener(onDismissListener)

        val titleV = dialog.findViewById<TextView>(R.id.dialog_title)
        titleV.text = title

        val addressInput = dialog.findViewById<TextInputLayout>(R.id.address_input)
        val postalInput = dialog.findViewById<TextInputLayout>(R.id.postal_input)
        val cityInput = dialog.findViewById<TextInputLayout>(R.id.city_input)

        addressInput.editText?.setText(address)
        postalInput.editText?.setText(postal)
        cityInput.editText?.setText(city)


        val send = dialog.findViewById<Button>(R.id.confirm)
        send.setOnClickListener {
            if (!FormUtil.checkInputLayoutsEmpty(activity.getString(R.string.error_field_required),
                    addressInput, postalInput, cityInput)) {
                dialog.dismiss()
                addressDialogListener.onAddressFilled(
                    addressInput.editText?.text.toString().trim { it <= ' ' },
                    postalInput.editText?.text.toString().trim { it <= ' ' },
                    cityInput.editText?.text.toString().trim { it <= ' ' }
                )
            }
        }

        val cancel = dialog.findViewById<View>(R.id.cancel) as Button
        cancel.setOnClickListener {
            addressDialogListener.onSkip()
            dialog.dismiss()
        }

        return dialog
    }


    fun buildConfirmationDialog(activity: Activity,
                                title: String,
                                description: String,
                                cancelText: String?,
                                confirmText: String?,
                                onConfirmationDialogClickListener: OnConfirmationDialogClickListener): Dialog {
        val dialog = Dialog(activity, R.style.Dialog)
        dialog.setContentView(R.layout.dialog_confirmation)

        val titleV = dialog.findViewById<TextView>(R.id.dialog_title)
        val descriptionV = dialog.findViewById<TextView>(R.id.dialog_description)

        titleV.text = title
        descriptionV.text = description

        val ok = dialog.findViewById<Button>(R.id.dialog_send)
        ok.setOnClickListener {
            dialog.dismiss()
            onConfirmationDialogClickListener.onConfirm()
        }
        if (confirmText != null) ok.text = confirmText

        val cancel = dialog.findViewById<Button>(R.id.dialog_cancel)
        if (cancelText != null) cancel.text = cancelText

        cancel.setOnClickListener {
            dialog.dismiss()
            onConfirmationDialogClickListener.onCancel()
        }

        return dialog
    }

    @JvmOverloads
    fun buildDatePickerDialog(activity: Activity,
                              calendar: Calendar = Calendar.getInstance(),
                              onDatePickedListener: OnDatePickedListener): Dialog {
        return DatePickerDialog(activity, R.style.Dialog, DatePickerDialog.OnDateSetListener { datePicker, i, i1, i2 -> onDatePickedListener.onDatePicked(i, i1, i2) }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    }

    interface OnTextInputDialogListener {
        fun onTextInput(text: String)
        fun onSkip()
    }

    interface OnNameSurnameDialogListener {
        fun onSkip()

        fun onNameSurnameInput(name: String, surname: String)
    }

    interface OnDatePickedListener {
        fun onDatePicked(year: Int, month: Int, day: Int)
    }

    interface OnConfirmationDialogClickListener {
        fun onConfirm()

        fun onCancel()
    }

    interface AddressDialogListener {
        fun onAddressFilled(address: String, postal: String, city: String)
        fun onSkip()

    }
}
