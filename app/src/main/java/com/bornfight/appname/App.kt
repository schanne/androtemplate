package com.bornfight.appname

import android.app.Application
import com.bornfight.common.dagger.components.ApplicationComponent
import com.bornfight.common.dagger.components.DaggerApplicationComponent
import com.bornfight.common.dagger.modules.ContextModule
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.firebase.NotificationChannelsHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

/**
 * Created by tomislav on 16/12/2016.
 */

class App : Application() {

    private var apiInterface: ApiInterface? = null

    lateinit var component: ApplicationComponent
        private set

    override fun onCreate() {
        super.onCreate()

        NotificationChannelsHelper.initChannels(this)

        this.component = DaggerApplicationComponent.builder()
            .contextModule(ContextModule(this))
            .build()

        apiInterface = component.apiInterface

        Glide.with(this).setDefaultRequestOptions(RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
    }

}
