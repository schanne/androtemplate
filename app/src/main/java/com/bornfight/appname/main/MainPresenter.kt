package com.bornfight.appname.main

import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvp.BaseContract
import com.bornfight.common.mvp.BasePresenter
import com.bornfight.common.session.SessionPrefImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by tomislav on 08/11/2016.
 */
class MainPresenter @Inject
constructor(compositeSubscription: CompositeDisposable, sessionPref: SessionPrefImpl,
            apiInterface: ApiInterface) : BasePresenter<MainContract.View>(compositeSubscription, sessionPref, apiInterface), MainContract.Presenter<MainContract.View> {

    override fun subscribe(view: MainContract.View) {
        super.subscribe(view)
        getDummy()
    }

    override fun getDummy() {
        view.showProgressCircle(true)
        compositeDisposable.add(
            apiInterface.login("demo")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<ApiInterface.LoginResponse>() {
                    override fun onSuccess(@NonNull loginResponse: ApiInterface.LoginResponse) {

                    }

                    override fun onError(@NonNull e: Throwable) {
                        handleError(e, object : BaseContract.Presenter.OnValidationErrorsListener {
                            override fun onValidationErrors(errors: ApiInterface.ValidationErrors) {
                                // dummy demo of getting the validation error
                                view.setDummy(errors.getFieldError("email", "\n"))
                            }
                        })
                    }
                }))
    }

}
