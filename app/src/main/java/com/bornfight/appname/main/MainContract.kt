package com.bornfight.appname.main

import com.bornfight.common.mvp.BaseContract

/**
 * Created by tomislav on 17/02/2017.
 */

interface MainContract {

    interface View : BaseContract.View {
        fun setDummy(string: String)
    }

    interface Presenter<V : BaseContract.View> : BaseContract.Presenter<V> {
        fun getDummy()
    }
}
