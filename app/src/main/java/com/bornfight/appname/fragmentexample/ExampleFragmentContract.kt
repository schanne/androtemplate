package com.bornfight.appname.fragmentexample

import com.bornfight.common.dagger.scopes.FragmentScope
import com.bornfight.common.mvp.BaseContract

/**
 * Created by tomislav on 17/02/2017.
 */

class ExampleFragmentContract {

    interface View : BaseContract.View {
        fun showDummy(dummy: String)
    }

    @FragmentScope
    interface Presenter<V : BaseContract.View> : BaseContract.Presenter<V> {
        fun getDummy()
    }
}
